class Question < ActiveRecord::Base
  attr_accessible :answer, :ask
  has_and_belongs_to_many :users
end
